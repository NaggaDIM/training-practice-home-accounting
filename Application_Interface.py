#!/usr/bin/python3
# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import QMainWindow, QMessageBox, QWidget, QTabWidget, QTableWidget, QTableWidgetItem, QPushButton, QLabel, QHBoxLayout, QVBoxLayout, QLineEdit, QComboBox, QDateTimeEdit, QGroupBox, QHeaderView
from PyQt5.QtGui import QPalette, QColor
from PyQt5.QtCore import pyqtSlot, Qt, QDate, QDateTime
import pyqtgraph as pg
import numpy as np
from datetime import datetime
import tzlocal


#Основное окно программы
class ApplicationInterface(QMainWindow):
	WINDOW_TITLE = 'Домашняя бухгалтерия'

	def __init__(self, controller):
		super().__init__()
		#Инициализация окна
		self.setWindowTitle(self.WINDOW_TITLE)
		self.controller = controller
		self.theme = self.controller.config_get_theme()
		self.palette = QPalette()


		#Добавление таб виджета
		self.app_widget = AppTabWidget(self)
		self.setCentralWidget(self.app_widget)
		self.toggle_theme()
		self.show()
	
	#Обновление таблицы
	def update_table(self):
		self.app_widget.main_tab.update_table()

	#Переключение темы оформления
	def toggle_theme(self):
		if self.theme == self.controller.THEME_DARK:
			self.palette.setColor(QPalette.Window, QColor(53, 53, 53))
			self.palette.setColor(QPalette.WindowText, Qt.white)
			self.palette.setColor(QPalette.Base, QColor(25, 25, 25))
			self.palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
			self.palette.setColor(QPalette.ToolTipBase, Qt.white)
			self.palette.setColor(QPalette.ToolTipText, Qt.white)
			self.palette.setColor(QPalette.Text, Qt.white)
			self.palette.setColor(QPalette.Button, QColor(53, 53, 53))
			self.palette.setColor(QPalette.ButtonText, Qt.white)
			self.palette.setColor(QPalette.BrightText, Qt.red)
			self.palette.setColor(QPalette.Link, QColor(235, 101, 54))
			self.palette.setColor(QPalette.Highlight, QColor(235, 101, 54))
			self.palette.setColor(QPalette.HighlightedText, Qt.black)
		elif self.theme == self.controller.THEME_LIGHT:
			self.palette.setColor(QPalette.Window, Qt.white)
			self.palette.setColor(QPalette.WindowText, Qt.black)
			self.palette.setColor(QPalette.Base, QColor(240, 240, 240))
			self.palette.setColor(QPalette.AlternateBase, Qt.white)
			self.palette.setColor(QPalette.ToolTipBase, Qt.white)
			self.palette.setColor(QPalette.ToolTipText, Qt.white)
			self.palette.setColor(QPalette.Text, Qt.black)
			self.palette.setColor(QPalette.Button, Qt.white)
			self.palette.setColor(QPalette.ButtonText, Qt.black)
			self.palette.setColor(QPalette.BrightText, Qt.red)
			self.palette.setColor(QPalette.Link, QColor(66, 155, 248))
			self.palette.setColor(QPalette.Highlight, QColor(66, 155, 248))
			self.palette.setColor(QPalette.HighlightedText, Qt.black)
		self.setPalette(self.palette)

#Виджет управления вкладками приложения
class AppTabWidget(QWidget):
	"""docstring for AppTabWidget"""
	def __init__(self, parent):
		super(QWidget, self).__init__(parent)
		#Инициализация объекта
		self.layout = QVBoxLayout()
		self.tabs = QTabWidget()

		#Инициализация вкладок
		self.main_tab = MainTab(parent)
		self.graph_tab = GraphTab(parent)
		self.settings_tab = SettingsTab(parent)

		self.tabs.resize(300,200)

		#Добавление вкладок
		self.tabs.addTab(self.main_tab, 'Главная')
		self.tabs.addTab(self.graph_tab, 'График')
		self.tabs.addTab(self.settings_tab, 'Настройки')

		self.layout.addWidget(self.tabs)
		self.setLayout(self.layout)

#
class MainTab(QWidget):
	"""docstring for MainTab"""
	BALANCE_MASK	= 'Баланс: {0:.2f}р.'
	INCOME_MASK		= 'Доходы: {0:.2f}р.'
	COSTS_MASK		= 'Расходы: {0:.2f}р.'

	BTN_UPD_LBL = 'Обновить'
	BTN_ADD_LBL = 'Добавить'
	BTN_EDT_LBL = 'Редактировать'
	BTN_DEL_LBL = 'Удалить'

	def __init__(self, parent):
		super().__init__()
		#Инициализация вкладки
		self.parent = parent
		self.controller = parent.controller
		self.layout = QHBoxLayout()
		self.table_headers = self.controller.get_table_headers()
		self.table_data = None
		self.table = QTableWidget(self)
		self.balance_lbl = QLabel()
		self.income_lbl = QLabel()
		self.costs_lbl = QLabel()
		self.start_date_inp = QDateTimeEdit()
		self.end_date_inp = QDateTimeEdit()
		self.init_ui()
		self.setLayout(self.layout)

	#Инициализация интерфейса
	def init_ui(self):
		self.table.setColumnCount(len(self.table_headers))
		self.table.setHorizontalHeaderLabels(self.table_headers)#ResizeToContents
		self.table.horizontalHeader().setSectionResizeMode(self.controller.ITEM_COL_TYPE, QHeaderView.ResizeToContents)
		self.table.horizontalHeader().setSectionResizeMode(self.controller.ITEM_COL_DESCRIPTION, QHeaderView.Stretch)
		self.table.horizontalHeader().setSectionResizeMode(self.controller.ITEM_COL_MODIFICATION, QHeaderView.ResizeToContents)
		self.table.horizontalHeader().setSectionResizeMode(self.controller.ITEM_COL_DATE, QHeaderView.ResizeToContents)

		self.update_table()
		self.layout.addWidget(self.table)

		controll_layout = QVBoxLayout()

		#Кнопка обновления списка элементов
		upd_btn = QPushButton(self.BTN_UPD_LBL)
		upd_btn.setMinimumWidth(200)
		upd_btn.setMaximumWidth(300)
		upd_btn.clicked.connect(self.update_table)
		upd_btn.setShortcut('Ctrl+U')

		#Кнопка добавления элемента
		add_btn = QPushButton(self.BTN_ADD_LBL)
		add_btn.clicked.connect(self.add_item_modal)
		add_btn.setShortcut('Ctrl+A')

		#Кнопка редактирования элемента
		edit_btn = QPushButton(self.BTN_EDT_LBL)
		edit_btn.clicked.connect(self.edit_item_modal)
		edit_btn.setShortcut('Ctrl+E')

		#Кнопка удаления элемента
		del_btn = QPushButton(self.BTN_DEL_LBL)
		del_btn.clicked.connect(self.del_item)
		del_btn.setShortcut('Ctrl+D')

		sort_date_box = QGroupBox('Показать за определённый период:')
		sort_date_lay = QVBoxLayout()

		sort_date_lay.addWidget(QLabel('Дата начала:'))
		self.start_date_inp.setMinimumDate(QDate.currentDate().addDays(-730))
		self.start_date_inp.setMaximumDate(QDate.currentDate().addDays(730))
		self.start_date_inp.setDisplayFormat("dd.MM.yyyy HH:mm:ss")
		sort_date_lay.addWidget(self.start_date_inp)

		sort_date_lay.addWidget(QLabel('Дата окончания:'))
		self.end_date_inp.setMinimumDate(QDate.currentDate().addDays(-730))
		self.end_date_inp.setMaximumDate(QDate.currentDate().addDays(730))
		self.end_date_inp.setDisplayFormat("dd.MM.yyyy HH:mm:ss")
		sort_date_lay.addWidget(self.end_date_inp)

		sort_btn = QPushButton('Применить')
		sort_btn.clicked.connect(self.update_table_sort)
		sort_date_lay.addWidget(sort_btn)

		reset_btn = QPushButton('Сбросить')
		reset_btn.clicked.connect(self.update_table)
		sort_date_lay.addWidget(reset_btn)

		sort_date_box.setLayout(sort_date_lay)

		controll_layout.addWidget(upd_btn)
		controll_layout.addWidget(add_btn)
		controll_layout.addWidget(edit_btn)
		controll_layout.addWidget(del_btn)
		controll_layout.addStretch(1)
		controll_layout.addWidget(sort_date_box)
		controll_layout.addStretch(5)
		controll_layout.addWidget(self.balance_lbl)
		controll_layout.addWidget(self.income_lbl)
		controll_layout.addWidget(self.costs_lbl)
		self.layout.addLayout(controll_layout)

	#Добавление элемента
	def add_item_modal(self):
		modal = ItemControlModal(self.parent, ItemControlModal.CREATE_MODE)

	#Редактирование элемента
	def edit_item_modal(self):
		selected_indexes = self.table.selectedIndexes()
		if selected_indexes:
			item = self.table_data[selected_indexes[0].row()]
			modal = ItemControlModal(self.parent, ItemControlModal.UPDATE_MODE, item)

	#Удаление элемента
	def del_item(self):
		selected_rows = self.table.selectedIndexes()
		if selected_rows:
			for item in selected_rows:
				self.controller.delete_item(self.table_data[item.row()][self.controller.ITEM_KEY_ID])
		self.update_table()

	#Обновление таблицы
	def update_table(self):		
		self.table_data = self.controller.get_data()
		self.table.setRowCount(len(self.table_data))
		if self.table is not None: 
			self.fill_table()
			self.update_control_labels()

	def update_table_sort(self):
		start_date_tmsmp = int(self.start_date_inp.dateTime().toMSecsSinceEpoch() / 1000)
		end_date_tmsmp = int(self.end_date_inp.dateTime().toMSecsSinceEpoch() / 1000)
		self.table_data = self.controller.get_data_with_sort(start_date_tmsmp, end_date_tmsmp)
		self.table.setRowCount(len(self.table_data))
		if self.table is not None: 
			self.fill_table()
			self.update_control_labels()

	#Заполнение таблицы и блокировка её от записи
	def fill_table(self):
		row = 0
		for item in self.table_data:
			for i in range(len(self.table_headers)):
				if i == self.controller.ITEM_COL_TYPE: data = self.get_item_type_info(item[self.controller.ITEM_KEY_TYPE])
				elif i == self.controller.ITEM_COL_DESCRIPTION: data = item[self.controller.ITEM_KEY_DESCRIPTION]
				elif i == self.controller.ITEM_COL_MODIFICATION: data = str(item[self.controller.ITEM_KEY_MODIFICATION])
				elif i == self.controller.ITEM_COL_DATE: data = datetime.fromtimestamp(item[self.controller.ITEM_KEY_DATE], tzlocal.get_localzone()).strftime('%d.%m.%Y %H:%M')
				else: data = ''
				cell = QTableWidgetItem(data)
				cell.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
				self.table.setItem(row, i, cell)
			row += 1

	#Получение описания типа
	def get_item_type_info(self, type):
		if type == self.controller.ITEM_TYPE_INCOME: return self.controller.ITEM_TYPE_LBL_INCOME
		elif type == self.controller.ITEM_TYPE_COSTS: return self.controller.ITEM_TYPE_LBL_COSTS
		else: return 'Неизвестный тип'

	#Обновление информационных данных
	def update_control_labels(self):
		balance = 0
		income = 0
		costs = 0
		for item in self.table_data:
			if item[self.controller.ITEM_KEY_TYPE] == self.controller.ITEM_TYPE_INCOME:
				income += item[self.controller.ITEM_KEY_MODIFICATION]
				balance += item[self.controller.ITEM_KEY_MODIFICATION]

			elif item[self.controller.ITEM_KEY_TYPE] == self.controller.ITEM_TYPE_COSTS:
				costs += item[self.controller.ITEM_KEY_MODIFICATION]
				balance -= item[self.controller.ITEM_KEY_MODIFICATION]

		self.balance_lbl.setText(self.BALANCE_MASK.format(balance))
		self.income_lbl.setText(self.INCOME_MASK.format(income))
		self.costs_lbl.setText(self.COSTS_MASK.format(costs))

#Окно добавления/изменения элемента
class ItemControlModal(QMainWindow):
	CREATE_MODE = 0
	UPDATE_MODE = 1

	def __init__(self, parent, mode, item = None):
		super().__init__(parent)
		#Инициализация окна
		self.parent = parent
		self.controller = parent.controller
		self.mode = mode
		self.item = item
		self.title = 'Добавление элемента' if mode == self.CREATE_MODE else 'Редактирование элемента'
		self.setFixedSize(400, 200)
		self.setWindowTitle(self.title)
		self.widget = QWidget()
		self.layout = QVBoxLayout()
		self.type_inp = QComboBox()
		self.desc_inp = QLineEdit()
		self.mod_inp = QLineEdit()
		self.date_inp = QDateTimeEdit()
		self.init_ui()
		self.widget.setLayout(self.layout)
		self.setCentralWidget(self.widget)
		self.setPalette(self.parent.palette)
		self.show()

	#Инициализация интерфейса
	def init_ui(self):
		#Поле тип
		type_lay = QHBoxLayout()
		type_lbl = QLabel('Тип:')
		type_lay.addWidget(type_lbl)
		type_lay.addStretch(1)
		self.type_inp.addItems(self.controller.get_item_types())
		self.type_inp.setMinimumWidth(300)
		type_lay.addWidget(self.type_inp)
		self.layout.addLayout(type_lay)

		#Поле описание
		desc_lay = QHBoxLayout()
		desc_lbl = QLabel('Описание:')
		desc_lay.addWidget(desc_lbl)
		desc_lay.addStretch(1)
		self.desc_inp.setMinimumWidth(300)
		desc_lay.addWidget(self.desc_inp)
		self.layout.addLayout(desc_lay)

		#Поле изменения
		mod_lay = QHBoxLayout()
		mod_lbl = QLabel('Изменение:')
		mod_lay.addWidget(mod_lbl)
		mod_lay.addStretch(1)
		self.mod_inp.setMinimumWidth(300)
		mod_lay.addWidget(self.mod_inp)
		self.layout.addLayout(mod_lay)

		#Поле даты
		date_lay = QHBoxLayout()
		date_lbl = QLabel('Дата:')
		date_lay.addWidget(date_lbl)
		date_lay.addStretch(1)
		self.date_inp.setMinimumWidth(300)
		self.date_inp.setMinimumDate(QDate.currentDate().addDays(-730))
		self.date_inp.setMaximumDate(QDate.currentDate().addDays(730))
		self.date_inp.setDisplayFormat("dd.MM.yyyy HH:mm:ss")
		date_lay.addWidget(self.date_inp)
		self.layout.addLayout(date_lay)

		#Заполнение данных в режиме редактирования
		if self.mode == self.UPDATE_MODE:
			self.type_inp.setCurrentIndex(self.item[self.controller.ITEM_KEY_TYPE])
			self.desc_inp.setText(self.item[self.controller.ITEM_KEY_DESCRIPTION])
			self.mod_inp.setText(str(self.item[self.controller.ITEM_KEY_MODIFICATION]))
			dateTime = QDateTime()
			dateTime.setTime_t(self.item[self.controller.ITEM_KEY_DATE])
			self.date_inp.setDateTime(dateTime)

		self.layout.addStretch(1)

		#Кнопка сохранить
		done_btn = QPushButton('Сохранить')
		done_btn.clicked.connect(self.done_event)
		self.layout.addWidget(done_btn)

		#Кнопка отмены
		cancel_btn = QPushButton('Отмена')
		cancel_btn.clicked.connect(self.cancel_event)
		self.layout.addWidget(cancel_btn)

	#Ивент кнопки сохранить
	def done_event(self):
		try:
			type = self.type_inp.currentIndex()
			description = self.desc_inp.text()
			modification = float(self.mod_inp.text())
			date = int(self.date_inp.dateTime().toMSecsSinceEpoch() / 1000)

			if not self.controller.ITEM_TYPE_INCOME <= type <= self.controller.ITEM_TYPE_COSTS: 
				QMessageBox.critical(self, "Ошибка", "Недопустимое значение типа")
				return

			if not description: 
				QMessageBox.critical(self, "Ошибка", "Недопустимое значение описания")
				return

			if not modification or modification < 0: 
				QMessageBox.critical(self, "Ошибка", "Недопустимое значение изменения\n  1. Число должно быть больше нуля\n  2. Дробная часть записывается через точку")
				return

			if self.mode == self.CREATE_MODE:
				self.parent.controller.insert_item(type, description, modification, date)
			elif self.mode == self.UPDATE_MODE:
				self.parent.controller.update_item(self.item[self.controller.ITEM_KEY_ID], type, description, modification, date)

			self.parent.update_table()
			self.close()
		except Exception as e:
			QMessageBox.critical(self, "Ошибка", "Недопустимое значение изменения\n  1. Число должно быть больше нуля\n  2. Дробная часть записывается через точку")
			return

	#Ивент кнопки отмена
	def cancel_event(self):
		self.close()

#Вкладка графика
class GraphTab(QWidget):
	"""docstring for GraphTab"""
	def __init__(self, parent):
		super().__init__()
		#Инициализация вкладки
		self.controller = parent.controller
		self.layout = QVBoxLayout()
		self.view  = pg.PlotWidget()
		self.curve = self.view.plot(name="Graph", symbolBrush=2)
		self.graph_data = None
		self.update_plot()
		self.init_ui()
		self.setLayout(self.layout)

	#Обновление графика
	def update_plot(self):
		self.graph_data = self.controller.get_graph_data()
		if self.graph_data:
			self.curve.setData(self.graph_data['x_values'], self.graph_data['y_values'])

	#Инициализация Интерфейса
	def init_ui(self):
		btn = QPushButton("Обновить график")
		btn.clicked.connect(self.update_plot)
		btn.setShortcut('Ctrl+G')
		self.layout.addWidget(self.view)
		self.layout.addWidget(btn)

#Вкладка настроек
class SettingsTab(QWidget):
	"""docstring for SettingsTab"""
	def __init__(self, parent):
		super().__init__()
		#Инициализация
		self.parent = parent
		self.controller = parent.controller
		self.layout = QVBoxLayout()

		self.db_filename_inp = QLineEdit()

		self.init_ui()
		self.setLayout(self.layout)

	#Инициализация интервейса
	def init_ui(self):
		ui_settings_box = QGroupBox('Внешний вид:')
		ui_settings_lay = QVBoxLayout()

		theme_lay = QHBoxLayout()
		theme_lbl = QLabel('Тема:')
		theme_lay.addWidget(theme_lbl)
		theme_lay.addStretch(1)
		theme_inp = QComboBox()
		theme_inp.addItems(self.controller.get_themes())
		theme_inp.setCurrentIndex(self.parent.theme)
		theme_inp.setMinimumWidth(700)
		theme_lay.addWidget(theme_inp)
		theme_inp.currentIndexChanged.connect(self.update_theme)

		ui_settings_lay.addLayout(theme_lay)
		ui_settings_box.setLayout(ui_settings_lay)

		db_settings_box = QGroupBox('База данных:')
		db_settings_lay = QVBoxLayout()

		filename_lay = QHBoxLayout()
		filename_lbl = QLabel('Имя файла:')
		filename_lay.addWidget(filename_lbl)
		filename_lay.addStretch(1)
		self.db_filename_inp.setMinimumWidth(615)
		self.db_filename_inp.setText(self.controller.config_get_db_name().replace(".db", ""))
		filename_lay.addWidget(self.db_filename_inp)
		db_done_filename_btn = QPushButton('Применить')
		db_done_filename_btn.clicked.connect(self.update_db_filename)
		filename_lay.addWidget(db_done_filename_btn)

		db_settings_lay.addLayout(filename_lay)

		reset_db_btn = QPushButton('Сбросить базу данных')
		reset_db_btn.clicked.connect(self.reset_db)
		db_settings_lay.addWidget(reset_db_btn)
		
		db_settings_box.setLayout(db_settings_lay)

		about_box = QGroupBox()
		about_lay = QHBoxLayout()

		dev_info_btn = QPushButton('О разработчике')
		dev_info_btn.clicked.connect(self.dev_info)
		dev_info_btn.setShortcut('Ctrl+N')
		app_info_btn = QPushButton('О программе')
		app_info_btn.clicked.connect(self.app_info)
		app_info_btn.setShortcut('Ctrl+P')
		help_btn = QPushButton('Помощь')
		help_btn.clicked.connect(self.help)
		help_btn.setShortcut('Ctrl+H')

		about_lay.addWidget(dev_info_btn)
		about_lay.addWidget(app_info_btn)
		about_lay.addWidget(help_btn)

		about_box.setLayout(about_lay)

		self.layout.addWidget(ui_settings_box)
		self.layout.addWidget(db_settings_box)
		self.layout.addStretch(1)
		self.layout.addWidget(about_box)

	#Обновление темы приложения
	def update_theme(self, index):
		if 0 <= index < len(self.controller.get_themes()):
			self.parent.theme = index
			self.controller.config_set_theme(index)
			self.parent.toggle_theme()

	#Обновление выбранной базы данных
	def update_db_filename(self):
		filename = self.db_filename_inp.text()
		self.controller.config_set_db_name(filename)
		QMessageBox.information(self,
			"Готово",
			"Изменения вступят в силу после перезагрузки приложения")

	def reset_db(self):
		self.controller.reset_db()
		self.parent.update_table()
		QMessageBox.information(self, "Готово", "База данных сброшена")

	def dev_info(self):
		QMessageBox().information(self,
				"О разработчике", 
				"Приложение разработано студентом Нижегородского Губернского Колледжа группы 31П:\n\t{}\nСайт разработчика: {}\n".format(
					'Смертиным Дмитрием Анатольевичем (NaggaDIM)',
					'http://naggadim.ru'
			))

	def app_info(self):
		QMessageBox().information(self,
				"О программе", 
				"{}\n{}\n\n{}\n\n{}\n\t{}\n\t{}\n\n{}".format(
					'Домашняя бухгалтерия – это система построения учета личных (семейных) доходов и расходов.',
					'Подразумевает ведение личного (семейного) бюджета.',
					'Актуальность представленного проекта заключается в том, что в настоящее время, всё ещё остаётся много желающих, которые хотели вести домашнюю бухгалтерию.',
					'Объект и предмет исследования:',
					'Объектом исследования являются семейные доходы и расходы',
					'Предметом исследования — это взаимодействия предоставленных данных в таблице',
					'Реализовано на ЯП - Python 3',
			))

	def help(self):
		QMessageBox.information(self,
			"Помощь",
			"{}\n{}\n{}\n{}\n{}\n{}\n\n{}\n{}\n{}\n{}\n\n{}\n{}\n{}\n{}\n{}".format(
				"Главный экран",
				"На данном экране представленны:",
				"  1. Таблица элементов",
				"  2. Блок клавишь управления элементами",
				"  3. Блок фильтрации элементов в определённом периоде",
				"  4. Информационные элементы",
				"Экран \"График\"",
				"На данном экране представленны:",
				"  1. График отображающий изменения бюджета (По всем имеющимся данным)",
				"  2. Клавиша обновления данных графика",
				"Экран \"Настройки\"",
				"На данном экране представленны:",
				"  1. Блок управления внешним видом приложения",
				"  2. Блок управления базой данных приложения",
				"  3. Блок информационных клавишь"

		))
#!/usr/bin/python3
# -*- coding: utf-8 -*-
import yaml
import sqlite3


class ApplicationController(object):
	"""docstring for ApplicationController"""
	#Ключи элемента
	ITEM_KEY_ID 			= 'id'
	ITEM_KEY_TYPE 			= 'type'
	ITEM_KEY_DESCRIPTION 	= 'description'
	ITEM_KEY_MODIFICATION 	= 'modification'
	ITEM_KEY_DATE 			= 'created_at'

	#Описание полей элемента
	ITEM_LBL_ID 			= 'ID'
	ITEM_LBL_TYPE 			= 'Тип'
	ITEM_LBL_DESCRIPTION 	= 'Описание'
	ITEM_LBL_MODIFICATION 	= 'Изменение'
	ITEM_LBL_DATE 			= 'Дата/Время'

	#Номер колонки для вывода элемента
	ITEM_COL_TYPE 			= 0
	ITEM_COL_DESCRIPTION 	= 1
	ITEM_COL_MODIFICATION 	= 2
	ITEM_COL_DATE 			= 3

	#Типы элемента
	ITEM_TYPE_INCOME 		= 0
	ITEM_TYPE_COSTS			= 1

	#Описание типов элемента
	ITEM_TYPE_LBL_INCOME 	= 'Доход'
	ITEM_TYPE_LBL_COSTS		= 'Расход'

	#Получение списка описаний типов элемента
	def get_item_types(self):
		return [
			self.ITEM_TYPE_LBL_INCOME,
			self.ITEM_TYPE_LBL_COSTS,
		]

	THEME_DARK = 0
	THEME_LIGHT = 1

	THEME_DARK_LABEL = 'Тёмная'
	THEME_LIGHT_LABEL = 'Светлая'

	def get_themes(self):
		return [
			self.THEME_DARK_LABEL,
			self.THEME_LIGHT_LABEL
		]
	

	#Таблица элементов
	DB_ACCOUTING_TABLE		= 'accouting'

	#Инициализация контроллера
	def __init__(self):
		super().__init__()
		self.configuration = self.get_configuration()
		self.db_connection = sqlite3.connect(self.config_get_db_name())
		self.validate_db()

	#Заголовки для таблицы элементов
	def get_table_headers(self):
		return [
			self.ITEM_LBL_TYPE,
			self.ITEM_LBL_DESCRIPTION,
			self.ITEM_LBL_MODIFICATION,
			self.ITEM_LBL_DATE,
		]

	#Получение настроек приложения
	def get_configuration(self):
		try:
			with open("config.yml", "r") as config_file:
				return yaml.load(config_file)
		except Exception as e:
			return {
				'app': { 'theme': self.THEME_DARK },
				'database': {'filename': 'default'},
			}

	#Получение темы приложения
	def config_get_theme(self):
		try:
			theme = self.configuration['app']['theme']
			if theme is not None and 0 <= theme <=1:
				return theme
			else:
				return 0
		except Exception as e:
			return 0

	def config_set_theme(self, theme: int):
		try:
			new_config = self.configuration
			new_config['app']['theme'] = theme
			with open("config.yml", 'w') as f:
				f.write(yaml.dump(new_config))
			self.configuration = new_config
		except Exception as e:
			return

	#Получение имени базы данных
	def config_get_db_name(self):
		try:
			return self.configuration['database']['filename'] + '.db'
		except Exception as e:
			return 'default.db'

	def config_set_db_name(self, filename):
		try:
			new_config = self.configuration
			new_config['database']['filename'] = filename
			with open("config.yml", 'w') as f:
				f.write(yaml.dump(new_config))
			self.configuration = new_config
		except Exception as e:
			return

	#Валидация базы данных
	def validate_db(self):
		try:
			version = self.get_db_version()
		except Exception as e:
			self.init_db()
			version = self.get_db_version()

		return version is not None and version >= 1

	#получение версии базы данных
	def get_db_version(self):
		c = self.db_connection.cursor()
		for item in c.execute("SELECT * FROM db_config WHERE key = 'db_version'"):
			return int(item[2])

	#Инициализация базы данных
	def init_db(self):
		cursor = self.db_connection.cursor()
		cursor.execute('''CREATE TABLE db_config (id INTEGER PRIMARY KEY AUTOINCREMENT, key TEXT, value TEXT)''')
		cursor.execute('''CREATE TABLE {} ({} INTEGER PRIMARY KEY AUTOINCREMENT, {} INTEGER, {} TEXT, {} REAL, {} TIMESTAMP)'''.format(self.DB_ACCOUTING_TABLE, self.ITEM_KEY_ID, self.ITEM_KEY_TYPE, self.ITEM_KEY_DESCRIPTION, self.ITEM_KEY_MODIFICATION, self.ITEM_KEY_DATE))
		cursor.execute('''INSERT INTO db_config ('key', 'value') VALUES('db_version', '1')''')
		self.db_connection.commit()

	def reset_db(self):
		cursor = self.db_connection.cursor()
		cursor.execute('''DELETE FROM {}'''.format(self.DB_ACCOUTING_TABLE))
		self.db_connection.commit()

	#Добавление элемента
	def insert_item(self, type: int, description: str, modification: float, created_at: int):
		cursor = self.db_connection.cursor()
		cursor.execute('''INSERT INTO {} ('{}', '{}', '{}', '{}') VALUES({}, '{}', {}, {})'''.format(self.DB_ACCOUTING_TABLE, self.ITEM_KEY_TYPE, self.ITEM_KEY_DESCRIPTION, self.ITEM_KEY_MODIFICATION, self.ITEM_KEY_DATE, type, description, modification, created_at))
		self.db_connection.commit()

	#Обновление элемента
	def update_item(self, id: int, type: int, description: str, modification: float, created_at: int):
		cursor = self.db_connection.cursor()
		cursor.execute('''UPDATE {} SET {} = {}, {} = '{}', {} = {}, {} = {} WHERE {} = {};'''.format(self.DB_ACCOUTING_TABLE, self.ITEM_KEY_TYPE, type, self.ITEM_KEY_DESCRIPTION, description, self.ITEM_KEY_MODIFICATION, modification, self.ITEM_KEY_DATE, created_at, self.ITEM_KEY_ID, id))
		self.db_connection.commit()

	#Удаление элемента
	def delete_item(self, id: int):
		cursor = self.db_connection.cursor()
		cursor.execute('''DELETE FROM {} WHERE {} = {}'''.format(self.DB_ACCOUTING_TABLE, self.ITEM_KEY_ID, id))
		self.db_connection.commit()

	#Получение списка элементов
	def get_data(self):
		cursor = self.db_connection.cursor()
		data = []
		for item in cursor.execute('''SELECT * FROM {} ORDER BY {}'''.format(self.DB_ACCOUTING_TABLE, self.ITEM_KEY_DATE)):
			data.append({
				self.ITEM_KEY_ID: item[0],
				self.ITEM_KEY_TYPE: item[1],
				self.ITEM_KEY_DESCRIPTION: item[2],
				self.ITEM_KEY_MODIFICATION: item[3],
				self.ITEM_KEY_DATE: item[4]
			})
		return data

	#Получение данных для графика
	def get_graph_data(self):
		data = self.get_data()
		graph_data = {'x_values': [], 'y_values': [], 'labels': []}
		balance = 0
		for item in data:
			graph_data['x_values'].append(item[self.ITEM_KEY_DATE])
			if item[self.ITEM_KEY_TYPE] == self.ITEM_TYPE_INCOME:
				balance += item[self.ITEM_KEY_MODIFICATION]
			elif item[self.ITEM_KEY_TYPE] == self.ITEM_TYPE_COSTS:
				balance -= item[self.ITEM_KEY_MODIFICATION]
			graph_data['y_values'].append(balance)
			graph_data['labels'].append(item[self.ITEM_KEY_DESCRIPTION])
		return graph_data

	#Получение элементов в период от старт до енд
	def get_data_with_sort(self, start: int, end: int):
		cursor = self.db_connection.cursor()
		data = []
		for item in cursor.execute('''SELECT * FROM {} WHERE ({} >= {} AND {} <= {}) ORDER BY {}'''.format(self.DB_ACCOUTING_TABLE, self.ITEM_KEY_DATE, start, self.ITEM_KEY_DATE, end, self.ITEM_KEY_DATE)):
			data.append({
				self.ITEM_KEY_ID: item[0],
				self.ITEM_KEY_TYPE: item[1],
				self.ITEM_KEY_DESCRIPTION: item[2],
				self.ITEM_KEY_MODIFICATION: item[3],
				self.ITEM_KEY_DATE: item[4]
			})
		return data
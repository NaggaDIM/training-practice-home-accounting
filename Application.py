#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
from PyQt5.QtWidgets import QApplication
from Application_Interface import ApplicationInterface
from Application_Controller import ApplicationController


if __name__ == '__main__':
	app = QApplication(sys.argv)
	app.setStyle("Fusion")
	ui = ApplicationInterface(ApplicationController())
	sys.exit(app.exec_())